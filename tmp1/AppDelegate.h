//
//  AppDelegate.h
//  tmp1
//
//  Created by Mikhail Baynov on 25/07/14.
//  Copyright (c) 2014 StarDreams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
